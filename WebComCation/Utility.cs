﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//using System.Threading.Tasks;

namespace WebComCation
{
    public static class Utility
    {
        public static string gblLocalStoredKey;
        public static string gblLastRundate;
        public static string gblExpiryDate;

        static Registry_Handler rg = new Registry_Handler();
        static ServerOutInMsg msg = new ServerOutInMsg();
        
        static Utility()
        {
            //msg = rg.GetStoredRegistryMsg();
            //gblLocalStoredKey = msg.LicenseKey;
            //gblLastRundate = Utility.DateTimeToString(Registry_Handler.GetLastRunDateFromReg());
            //gblExpiryDate = msg.EndDate;
        }



        /// <summary>
        /// Parameter dataString should be in the format of (YYYY MM DD HH MM SS)
        /// Ex. 20170921125936 for 21-sep-2017 12:36
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static DateTime StringToDateTime(string dateString)
        {
            if(dateString ==null || dateString.Trim().Length==0)
            {
                //set to invalid date
                //throw new Exception("Date processing failed");

                dateString = "20000921125936";

            }

            //===================Other Format 2018-09-04==============

            if(dateString.Length==10 && dateString.Contains("-"))
            {
                dateString = dateString.Replace("-", "") + "000000"; // manual HH:MM:SS
            }


            //========================================================


            List<int> arrayInt = new List<int>();
            int indcnt = 0;
            foreach (char c in dateString)
            {
                if (indcnt <= 13)
                {
                    string s = dateString[indcnt].ToString();
                    arrayInt.Add(Convert.ToInt32(s));
                }
                indcnt++;
            }

            int year, month, day, hour, min, sec;
            string strYear, strMonth, strDay, strHour, strMin, strSec;

            DateTime dt;
            if (arrayInt.Count == 14)
            {
                strYear = arrayInt[0].ToString() + arrayInt[1].ToString() + arrayInt[2].ToString() + arrayInt[3].ToString();
                strMonth = arrayInt[4].ToString() + arrayInt[5].ToString();
                strDay = arrayInt[6].ToString() + arrayInt[7].ToString();
                strHour = arrayInt[8].ToString() + arrayInt[9].ToString();
                strMin = arrayInt[10].ToString() + arrayInt[11].ToString();
                strSec = arrayInt[12].ToString() + arrayInt[13].ToString();

                year = Convert.ToInt32(strYear);
                month = Convert.ToInt32(strMonth);
                day = Convert.ToInt32(strDay);
                hour = Convert.ToInt32(strHour);
                min = Convert.ToInt32(strMin);
                sec = Convert.ToInt32(strSec);
                dt = new DateTime(year, month, day, hour, min, sec);
            }
            else
            {
                throw new Exception("string not in a correct format");
            }
            //DateTime dt = new DateTime((year, month, day, hour, min, sec);
            return dt;
        }

        /// <summary>
        /// This function will convert the given DateTime Instance in string like "20170921125936" for 21-Sep-17 12:59
        /// </summary>
        /// <param name="dt">DateTime Instance to be converted to string value</param>
        /// <returns></returns>
        public static string DateTimeToString(DateTime dt)
        {
            string strYear, strMonth, strDay, strHour, strMin, strSec;
            if (dt.Month.ToString().Length == 1)
                strMonth = "0" + dt.Month.ToString();
            else
                strMonth = dt.Month.ToString();
            //////////
            if (dt.Day.ToString().Length == 1)
                strDay = "0" + dt.Day.ToString();
            else
                strDay = dt.Day.ToString();

            /////////

            if (dt.Hour.ToString().Length == 1)
                strHour = "0" + dt.Hour.ToString();
            else
                strHour = dt.Hour.ToString();
            ////////
            if (dt.Minute.ToString().Length == 1)
                strMin = "0" + dt.Minute.ToString();
            else
                strMin = dt.Minute.ToString();

            ///////
            if (dt.Second.ToString().Length == 1)
                strSec = "0" + dt.Second.ToString();
            else
                strSec = dt.Second.ToString();


            // return dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second; 
            return dt.Year.ToString() + strMonth + strDay + strHour + strMin + strSec;

        }

        public static DateTime FindGreatesDate(DateTime dtF, DateTime dtS)
        {
            int y1 = dtF.Year;
            int m1 = dtF.Month;
            int d1 = dtF.Day;

            int y2 = dtS.Year;
            int m2 = dtS.Month;
            int d2 = dtS.Day;

            if (y1 > y2)
            {
                return dtF;
            }
            else if (y1 == y2)
            {
                if (m1 > m2)
                {
                    return dtF;

                }
                else if (m1 == m2)
                {
                    if (d1 >= d2)
                    {
                        return dtF;
                    }
                    else
                    {
                        return dtS;
                    }
                }
                else
                {
                    return dtS;
                }
            }
            else
            {
                return dtS;
            }


        }
        /// <summary>
        /// Compares upto day only (minutes and Seconds will not be considered)
        /// </summary>
        /// <param name="dtF"></param>
        /// <param name="dtS"></param>
        /// <returns></returns>
        public static bool IsDateQqual(DateTime dtF, DateTime dtS)
        {
            int y1 = dtF.Year;
            int m1 = dtF.Month;
            int d1 = dtF.Day;

            int y2 = dtS.Year;
            int m2 = dtS.Month;
            int d2 = dtS.Day;

            if (y1 == y2)
                if (m1 == m2)
                    if (d1 == d2)
                    {
                        return true;
                    }
            return false;
        }
        public static bool IsFirstDateGreaterOrEqual(DateTime dtF, DateTime dtS)
        {
            int y1 = dtF.Year;
            int m1 = dtF.Month;
            int d1 = dtF.Day;
            int h1 = dtF.Hour;
            int min1 = dtF.Minute;
            int s1 = dtF.Second;

            int y2 = dtS.Year;
            int m2 = dtS.Month;
            int d2 = dtS.Day;
            int h2 = dtS.Hour;
            int min2 = dtS.Minute;
            int s2=dtS.Second;

            if (y1 > y2)
            {
                return true;
            }
            else if (y1 == y2)
            {
                if (m1 == m2)
                {
                    if (d1 >= d2)
                    {
                        if (h1 > h2)
                        {
                            //if (min1 > min2)
                            {
                                return true;
                            }
                        }
                        else if (h1==h2)
                        {
                            if ((min1-1) > min2)
                            {
                                return true;
                            }
                            
                        }
                        //return true;
                    }
                    
                }
                else if (m1 > m2)
                {
                    return true;
                }

            }
            return false;
        }

        public static string ConvertToEncrypt(string valueToEncrypt)
        {
            byte[] bt = UTF8Encoding.UTF8.GetBytes(valueToEncrypt);
            string s = Convert.ToBase64String(bt);
            return s;
        }
        public static string ConvertToStringFromEncrypt(string EncryptedValue)
        {
            byte[] pt = Convert.FromBase64String(EncryptedValue);
            string op = UTF8Encoding.UTF8.GetString(pt);
            return op;
        }
        public static string ConvertToStringFromEncrypt52(string EncryptedValue)
        {
            byte[] pt = Convert.FromBase64String(EncryptedValue);
            string op = UTF8Encoding.UTF8.GetString(pt);
            return op;
        }

        /// <summary>
        /// Implemented by sumit on 16-Sep-2018 for checking if PC is connected with internet or not
        /// </summary>
        /// <returns>if connected returns true, if not connected then returns false</returns>
        public static bool IsInternetConnected()
        {

            using (var client = new WebClient())
            {
                try
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                        return true;

                    }
                }
                catch (Exception ex1)
                {                   
                    return false;
                }
            }

        }
    }
    
}
