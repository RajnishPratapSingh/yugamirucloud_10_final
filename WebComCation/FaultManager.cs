﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Net.Configuration;
using System.Net.Mail;

/// <summary>
/// Created by sumit for GSP-775 on 28-Aug-18.
/// </summary>






namespace WebComCation
{
    /// <summary>
    /// This class is designed to receive exception and can log the exception details.
    /// Loging methods are Message Dialog, File Writing, windows application event, local file writing, sending the details on email.
    /// These logging methods can be turned OFF/ON from App.config file. 
    /// If email sending feature is Turned on then  Sender and receiver email addresses should be in App.config file.
    /// </summary>
    public class FaultManager
    {
       
        static FaultManager()
        {
            setFeatureFlagsFromConfig();
        }



        /// <summary>
        /// default settings are set for gmail, for other modification may be required. 
        /// For receiving, in gmail account settings 'allow access less secure app' should be enabled
        /// </summary>
        /// <param name="FromEmailAddress"></param>
        /// <param name="ToEmailAddress"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static bool SendEmail(string FromEmailAddress, string ToEmailAddress,Exception ex)
        {
            ESubject = "Yugamiru Cloud Fault Manager: " + DateTime.Now.ToString() + " [PC Name]: " + Environment.MachineName + " [Window User Name]: " + Environment.UserName;
            bool result = false;

            MailMessage mail = new MailMessage(FromEmailAddress, ToEmailAddress);
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            mail.Subject = ESubject;

            mail.Body = ex.Message + Environment.NewLine + ex.StackTrace;
            client.Credentials = new System.Net.NetworkCredential(EmailAccountUserName, EmailAccountPwd);
            try
            {
                client.Send(mail);
            }
            catch(Exception exmail)
            {
                WriteToFile(exmail);
                return false;
            }
            result = true;
            return result;
        }



        private static bool WriteToFile(Exception ex)
        {
           string  lineOne = "Yugamiru Cloud Fault Manager: " + DateTime.Now.ToString() + " [PC Name]:" + Environment.MachineName + " [Window User Name]:" + Environment.UserName+Environment.NewLine;
            bool result = false;


            string Content = @"===========================================================================================" + Environment.NewLine;
            Content += lineOne + Environment.NewLine;
            Content+= @"---------------------------------------------------------------------------------------------" + Environment.NewLine;
            Content += ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine;
            Content += @"===========================================================================================" + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
            File.AppendAllText("C:\\ProgramData\\gsport\\Yugamiru cloud\\YugamiruFaultManager", Content);
            result = true;
            return result;
        }


        /// <summary>
        /// This message reads the App.config file and sets the flags and Email addresses
        /// </summary>
        private static void setFeatureFlagsFromConfig()
        {
            string filewrite = System.Configuration.ConfigurationManager.AppSettings.Get("filewrite");
            if (filewrite != null && filewrite.Trim().Length > 0)
            {
                if (filewrite.ToUpper() == "TRUE")
                {
                    LogInFile = true;
                }
                else if (filewrite.ToUpper() == "FALSE")
                {
                    LogInFile = false;
                }
            }

            string showMsg = System.Configuration.ConfigurationManager.AppSettings.Get("showMsg");
            if (showMsg != null && showMsg.Trim().Length > 0)
            {
                if (showMsg.ToUpper() == "TRUE")
                {
                     LogShowMessageBox= true;
                }
                else if (showMsg.ToUpper() == "FALSE")
                {
                    LogShowMessageBox = false;
                }
            }

            string sendEmail = System.Configuration.ConfigurationManager.AppSettings.Get("sendEmail");
            if (sendEmail != null && sendEmail.Trim().Length > 0)
            {
                if (sendEmail.ToUpper() == "TRUE")
                {
                    LogInSendEmail = true;
                }
                else if (sendEmail.ToUpper() == "FALSE")
                {
                    LogInSendEmail = false;
                }
            }

            string winEvent = System.Configuration.ConfigurationManager.AppSettings.Get("winEvent");
            if (winEvent != null && winEvent.Trim().Length > 0)
            {
                if (winEvent.ToUpper() == "TRUE")
                {
                    LogInWinEvent = true;
                }
                else if (winEvent.ToUpper() == "FALSE")
                {
                    LogInWinEvent = false;
                }
            }


            string fromAd = System.Configuration.ConfigurationManager.AppSettings.Get("fromAd");
            if (fromAd != null && fromAd.Trim().Length > 0)
            {
                if (fromAd.Contains("@"))
                {
                    FromAddress = fromAd;
                }
                
            }

            string ToAd = System.Configuration.ConfigurationManager.AppSettings.Get("ToAd");
            if (ToAd != null && ToAd.Trim().Length > 0)
            {
                if (ToAd.Contains("@"))
                {
                    ToAddress = fromAd;
                }
            }

            string portNum = System.Configuration.ConfigurationManager.AppSettings.Get("portNum");
            if (portNum != null && portNum.Trim().Length > 0)
            {
                HostPortNumber = portNum;
            }

            string emailUserName = System.Configuration.ConfigurationManager.AppSettings.Get("emailUserName");
            if (emailUserName != null && emailUserName.Trim().Length > 0)
            {
                EmailAccountUserName = emailUserName;
            }

            string emailPassword = System.Configuration.ConfigurationManager.AppSettings.Get("emailPassword");
            if(emailPassword != null && emailPassword.Trim().Length>0)
            {
                EmailAccountPwd = emailPassword;
            }
        }

        public static void LogIt(Exception ex)
        {


            string FaultManagerEnabled = System.Configuration.ConfigurationManager.AppSettings.Get("FaultManagerEnabled");
            if (FaultManagerEnabled != null && FaultManagerEnabled.Trim().Length > 0)
            {
                if (FaultManagerEnabled.ToUpper() == "TRUE")
                {
                    
                }
                else if (FaultManagerEnabled.ToUpper() == "FALSE")
                {
                    return;
                }
            }

            if (LogInFile)
            {
                WriteToFile(ex);
            }
            if(LogInSendEmail)
            {
                SendEmail(FromAddress, ToAddress, ex);
            }
        }

        #region Properties
        private static string ESubject
        {
            get;
            set;
        }
        private static bool LogInFile { get; set; }
        private static bool LogShowMessageBox { get; set; }
        private static bool LogInSendEmail { get; set; }
        private static bool LogInWinEvent { get; set; }
        private static string FromAddress { get; set; }
        private static string ToAddress { get; set; }
        private static string HostPortNumber { get; set; }
        private static string EmailAccountUserName { get; set; }
        private static string EmailAccountPwd { get; set; }

        #endregion
    }
}
