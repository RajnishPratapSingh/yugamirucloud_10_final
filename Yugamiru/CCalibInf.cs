﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{

    public class CCalibInf
    {
        public long[,] FPLpos = new long[4, 2];
        public long[] FPLcenter = new long[2];
        public long[,] Anklepos = new long[2,2];
        public double CameraAngle;
        public int[,] MidLineEdge = new int[2, 2];

        public CCalibInf(  )
 {
     Initialize();
 }

 public CCalibInf( CCalibInf rSrc )
{
	FPLcenter[0] = rSrc.FPLcenter[0];
	FPLcenter[1] = rSrc.FPLcenter[1];
	Anklepos[0,0] = rSrc.Anklepos[0,0];
	Anklepos[0,1] = rSrc.Anklepos[0,1];
	Anklepos[1,0] = rSrc.Anklepos[1,0];
	Anklepos[1,1] = rSrc.Anklepos[1,1];
}

~CCalibInf(  )
{
}

/*public CCalibInf &operator=( const CCalibInf &rSrc )
{
    FPLcenter[0] = rSrc.FPLcenter[0];
    FPLcenter[1] = rSrc.FPLcenter[1];
    Anklepos[0][0] = rSrc.Anklepos[0][0];
    Anklepos[0][1] = rSrc.Anklepos[0][1];
    Anklepos[1][0] = rSrc.Anklepos[1][0];
    Anklepos[1][1] = rSrc.Anklepos[1][1];
    return *this;
}*/

 public void Initialize( )
{
    FPLcenter[0] = 512;
    FPLcenter[1] = 1280 * 2 / 10;
    Anklepos[0,0] = 612;
    Anklepos[0,1] = 283;//1280 * 2/10;
    Anklepos[1,0] = 412;
    Anklepos[1,1] = 283;//1280 * 2/10;	
    CameraAngle = 0;
}

public void CalcFPLcenter()
{

    FPLcenter[0] = 512;
    FPLcenter[1] = 283;//1280 * 1 / 10;

}

public long GetFPLposX(int iIndex) 
{
	if ( iIndex< 0 ){
		return 0;
	}
	if ( iIndex >= 4 ){
		return 0;
	}
	return FPLpos[iIndex,0];
}

public long GetFPLposY(int iIndex) 
{
	if ( iIndex< 0 ){
		return 0;
	}
	if ( iIndex >= 4 ){
		return 0;
	}
	return FPLpos[iIndex , 1];
}

public long GetFPLcenterX() 
{
	return FPLcenter[0];
}

public long GetFPLcenterY() 
{
	return FPLcenter[1];
}
public long GetRightAnkleposX() 
{
	return Anklepos[0,0];
}

public long GetRightAnkleposY()
{
	return Anklepos[0,1];
}

public long GetLeftAnkleposX()
{
	return Anklepos[1,0];
}

public long GetLeftAnkleposY() 
{
	return Anklepos[1,1];
}

public double GetCameraAngle( )
{
	return CameraAngle;
}

public int GetMidLineEdgeX(int iIndex) 
{
	if ( iIndex< 0 ){
		return 0;
	}
	if ( iIndex >= 2 ){
		return 0;
	}
	return MidLineEdge[iIndex,0];
}

public int GetMidLineEdgeY(int iIndex) 
{
	if ( iIndex< 0 ){
		return 0;
	}
	if ( iIndex >= 2 ){
		return 0;
	}
	return MidLineEdge[iIndex,1];
}

    }
}
