﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MuscleStateInfo
    {
        List<MuscleStateInfoElement> m_listMuscleStateInfoElement = new List<MuscleStateInfoElement>();
        // ‹Ø“÷ó‘Ôî•ñ.
     public MuscleStateInfo()
{
	m_listMuscleStateInfoElement.Clear();
}

  

public void Clear()
{
    m_listMuscleStateInfoElement.Clear();
}

public void AddData(int iMuscleID, int iMuscleStatePatternID, int iMuscleStateTypeID, int iMuscleStateLevel)
{
    MuscleStateInfoElement MuscleStateInfoElement = new MuscleStateInfoElement(iMuscleID, iMuscleStatePatternID, iMuscleStateTypeID, iMuscleStateLevel);
    m_listMuscleStateInfoElement.Add(MuscleStateInfoElement);
}

public double ApplyOperator(int iOperatorID, int iMuscleID, int iMuscleStateTypeID) 
{
	double dValue = 0.0;
            /*
std::list<CMuscleStateInfoElement>::const_iterator it = m_listMuscleStateInfoElement.begin();
	for( it = m_listMuscleStateInfoElement.begin(); it != m_listMuscleStateInfoElement.end(); it++ ){
		if (( it->GetMuscleID() == iMuscleID ) && ( (* it).GetMuscleStateTypeID() == iMuscleStateTypeID )){
			if ( iOperatorID == COLLECTOPERATORID_GETSUM ){
				dValue += it->GetMuscleStateLevel();		
			}
			else if ( iOperatorID == COLLECTOPERATORID_GETMAX ){
				if ( (* it).GetMuscleStateLevel() > dValue ){
					dValue = it->GetMuscleStateLevel();
				}			
			}
		}
	}*/
    foreach(var Element in m_listMuscleStateInfoElement)
            {
                if((Element.GetMuscleID() == iMuscleID) && (Element.GetMuscleStateTypeID() == iMuscleStateTypeID))
                {
                    if (iOperatorID == Constants.COLLECTOPERATORID_GETSUM)
                        dValue += Element.GetMuscleStateLevel();
                    else if(iOperatorID == Constants.COLLECTOPERATORID_GETMAX)
                    {
                        if(Element.GetMuscleStateLevel() > dValue)
                        {
                            dValue = Element.GetMuscleStateLevel();
                        }
                    }
                }
            }
	return dValue;
}

    }
}
