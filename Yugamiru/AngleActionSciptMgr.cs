﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    public class AngleActionSciptMgr
    {
        int m_iCount;
        AngleActionScriptElement[] m_pElement;
        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // Šp“xƒAƒNƒVƒ‡ƒ“ƒXƒNƒŠƒvƒgƒ}ƒXƒ^[ƒf[ƒ^.
        public AngleActionSciptMgr()
        {
            m_iCount = 0;
            m_pElement = new AngleActionScriptElement[m_iCount];

        }
       


        public bool ReadFromFile(string lpszFolderName, string lpszFileName, string pchErrorFilePath /* = NULL */ )
        {
            char[] buf = new char[1024];
            List<AngleActionScriptElement> listElement = new List<AngleActionScriptElement>();


            if (m_pElement != null)
            {

                m_pElement = null;
            }
            m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }

            string strError = string.Empty;
            int iLineNo = 1;
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(ref buf, 1024);
                //var t = File.ReadAllLines(@"Resources\ResultAction.inf");
                AngleActionScriptElement Element = new AngleActionScriptElement();

                int iErrorCode = Element.ReadFromString(ref buf);
                if (iErrorCode == Constants.READFROMSTRING_SYNTAX_OK)
                {
                    //listElement.push_back(Element);
                    listElement.Add(Element);
                }
                else
                {
                    m_SymbolFunc.OutputReadErrorString(ref strError, iLineNo, iErrorCode);
                }
                iLineNo++;
            }
            if (pchErrorFilePath != null)
            {
                StreamWriter fp = new StreamWriter(pchErrorFilePath);
                fp.Dispose();

            }

            // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
            int iSize = listElement.Count;
            if (iSize <= 0)
            {
                return true;
            }
            m_pElement = new AngleActionScriptElement[iSize];
            if (m_pElement == null)
            {
                return false;
            }
            m_iCount = iSize;

            //int i = 0;
            //List<ResultActionScriptElement> iterator index;
            for (int j = 0; j < listElement.Count; j++)
            {
                m_pElement[j] = listElement[j];
            }

            return true;
        }

        public bool ResolveRange(RealValueRangeDefinitionMgr RangeDefinitionMgr, string pchErrorFilePath /* = NULL */ )
        {
            bool bRet = true;
            string strError = string.Empty;
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (m_pElement[i].m_iScriptOpecodeID == Constants.ANGLESCRIPTOPECODEID_CHECK)
                {
                    string strRangeSymbol = m_pElement[i].m_strRangeSymbol;
                    int iMinValueOperatorID = 0;
                    int iMaxValueOperatorID = 0;
                    double dMinValue = 0.0;
                    double dMaxValue = 0.0;
                    if (RangeDefinitionMgr.GetRange(ref iMinValueOperatorID, ref iMaxValueOperatorID,ref dMinValue, ref dMaxValue, strRangeSymbol))
                    {
                        m_pElement[i].ResolveRange( iMinValueOperatorID,  iMaxValueOperatorID,  dMinValue,  dMaxValue);
                    }
                    else
                    {
                        string strTmp;
                        strTmp = "Symbol %s is undefined\n" + strRangeSymbol;
                        strError += strTmp;
                        bRet = false;
                    }
                }
            }
            if (pchErrorFilePath != null)
            {
                StreamWriter sw = new StreamWriter(pchErrorFilePath);
                sw.Write(strError);
                sw.Dispose();
            }
            return bRet;
        }

        public bool Execute(MuscleStateInfo MuscleStateInfo, FrontBodyAngle Angle, SideBodyAngle AngleSide, MuscleStatePatternDefinitionMgr MuscleStatePatternDefinitionMgr)
        {
            bool bContextFlag = true;
            MuscleStateInfo.Clear();
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (m_pElement[i].m_iScriptOpecodeID == Constants.ANGLESCRIPTOPECODEID_START)
                {
                    switch (m_pElement[i].m_iBodyPositionTypeID)
                    {
                        case Constants.BODYPOSITIONTYPEID_STANDING:
                            bContextFlag = (Angle.IsStanding());
                            break;
                        case Constants.BODYPOSITIONTYPEID_KNEEDOWN:
                            bContextFlag = !(Angle.IsStanding());
                            break;
                        case Constants.BODYPOSITIONTYPEID_COMMON:
                            bContextFlag = true;
                            break;
                        default:
                            bContextFlag = false;
                            break;
                    }
                }
                else if (m_pElement[i].m_iScriptOpecodeID == Constants.ANGLESCRIPTOPECODEID_CHECK)
                {
                    int iBodyAngleID = m_pElement[i].m_iBodyAngleID;
                    // strAngleSymbol‚©‚ç•Ï”‚Ì’l‚ðdValue‚É‹‚ß‚é.			
                    double dValue = m_SymbolFunc.GetBodyAngleValue(Angle, AngleSide, iBodyAngleID);
                    if (!(m_pElement[i].IsSatisfied(dValue)))
                    {
                        bContextFlag = false;
                    }
                }
                else if (m_pElement[i].m_iScriptOpecodeID == Constants.ANGLESCRIPTOPECODEID_END)
                {
                    if (bContextFlag)
                    {
                        int iMuscleStatePatternID = m_pElement[i].m_iMuscleStatePattenID;
                        int iMuscleStateLevel = m_pElement[i].m_iMuscleStateLevel;
                        MuscleStatePatternDefinitionMgr.GetMuscleStateInfo(ref MuscleStateInfo, iMuscleStatePatternID, iMuscleStateLevel);
                    }
                }
            }
            return true;
        }

    }
}
