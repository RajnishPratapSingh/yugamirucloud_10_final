﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class IDD_ABOUTBOX : Form
    {
        public IDD_ABOUTBOX()
        {
            InitializeComponent();
            this.Text = Yugamiru.Properties.Resources.KEYWORD_VERSION;
            label1.Text = Yugamiru.Properties.Resources.KEYWORD_JOINTEDIT;
            label2.Text = Yugamiru.Properties.Resources.KEYWORD_COPYRIGHT;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void IDD_ABOUTBOX_Load(object sender, EventArgs e)
        {

        }
    }
}
