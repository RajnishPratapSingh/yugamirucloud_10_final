﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

public class PrinterBounds
{
    [DllImport("gdi32.dll")]
    private static extern Int32
     GetDeviceCaps(IntPtr hdc, Int32 capindex);
    private const int PHYSICALWIDTH = 110;
    private const int PHYSICALHEIGHT = 111;
    private const int PHYSICALOFFSETX = 112;
    private const int PHYSICALOFFSETY = 113;  
    public readonly Rectangle Bounds;
    
    public PrinterBounds(PrintPageEventArgs e)
    {
        IntPtr hDC = e.Graphics.GetHdc();

        int width = GetDeviceCaps(hDC, PHYSICALWIDTH); // physical width in device units
        int height = GetDeviceCaps(hDC,PHYSICALHEIGHT);// physical height in device units

        width = width - GetDeviceCaps(hDC,PHYSICALOFFSETX) * 2;
        height = height - GetDeviceCaps(hDC,PHYSICALOFFSETY) * 2;
        e.Graphics.ReleaseHdc();
        Bounds = new Rectangle(0,0,width,height);
    }
}
